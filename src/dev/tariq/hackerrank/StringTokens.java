package dev.tariq.hackerrank;

/**
 * @author tariq Aug 11, 2019 2:56:41 PM
 *
 */
public class StringTokens {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 String s = "He is a very very good boy, isn't he?";
		 try{
			 	s = s.trim();
		        if (s.length() == 0) {
		            System.out.print(0);
		        } else if(s.length() <= 400000){
		            String words[] = s.split("[ !,?.\\_'@]+");
		            System.out.println(words.length);
		            for (String m : words) {
		                System.out.println(m);
		            }
		        }
		 }catch(java.util.NoSuchElementException e){
		     System.out.println(0);
		 }
	}

}
