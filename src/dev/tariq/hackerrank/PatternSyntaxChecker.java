package dev.tariq.hackerrank;

import java.util.regex.Pattern;

/**
 * @author tariq Aug 11, 2019 3:12:38 PM
 *
 */
public class PatternSyntaxChecker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int testCases = 3;
		String pattern1 = "([A-Z])(.+)";
		String pattern2 = "[AZ[a-z](a-z)";
		String pattern3 = "batcatpat(nat";
		while(testCases>0){
          	//Write your code
			boolean valid = true;
			try {
				Pattern.compile(pattern1);
			} catch (Exception e) {
				valid = false;
			}
			if(valid) {
				System.out.println("Valid");
			}else {
				System.out.println("Invalid");
			}
			testCases--;
		}
	}

}
