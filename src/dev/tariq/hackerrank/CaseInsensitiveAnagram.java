package dev.tariq.hackerrank;

/**
 * @author tariq Aug 11, 2019 1:54:58 PM
 *
 */
public class CaseInsensitiveAnagram {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String a = "anagram";
		String b = "margana";
        System.out.println( (isAnagramJavaStream(a, b)) ? "Anagrams" : "Not Anagrams" );
        System.out.println( (isAnagramForLoop(a, b)) ? "Anagrams" : "Not Anagrams" );
        System.out.println( (isAnagramArraysSort(a, b)) ? "Anagrams" : "Not Anagrams" );
        System.out.println( (isAnagramExclusiveOR(a, b)) ? "Anagrams" : "Not Anagrams" );

	}
	
	private static boolean initialCheck(String a, String b) {
		//Check invalid input   
		if( a == null || b == null || a.equals("") || b.equals("") )
		    throw new IllegalArgumentException();

		//initial length check
		if ( a.length() != b.length() )
	    return false;
		
		return true;
	}
	
	private static java.util.Map<Integer, Long> charFreq(final String s) {
		  return s.toLowerCase().chars().boxed()
		    .collect(java.util.stream.Collectors.groupingBy( 
		      java.util.function.Function.identity(),
		      java.util.stream.Collectors.counting()
		    ));
	}
	
	private static boolean isAnagramJavaStream(final String a, final String b) {
			if(!initialCheck(a, b)) {
				return false;
			}
		  return charFreq(a).equals(charFreq(b));
	}
	
	private static boolean isAnagramForLoop(String a, String b) {
		
		if(!initialCheck(a, b)) {
			return false;
		}

		a = a.toLowerCase();
		b = b.toLowerCase();
		
	    int index;
	    for(int i=0;i<a.length();i++)
	    {
	        index =b.indexOf(a.charAt(i));
	        if(index==-1)
	            return false;
	        else
	        {
	            //this removes b[index]
	            b = b.substring(0,index) + b.substring(index+1);
	        }
	    }
	    return true;
	}
	
	private static boolean isAnagramArraysSort(String a, String b) {
		
		if(!initialCheck(a, b)) {
			return false;
		}

		a = a.toLowerCase();
		b = b.toLowerCase();

		char[] x = a.toCharArray();
		char[] y = b.toCharArray();
		java.util.Arrays.sort(x);
		java.util.Arrays.sort(y);
		return (new String(x)).equals(new String(y));

	}
	
	private static boolean isAnagramExclusiveOR(String a, String b) {
		
		if(!initialCheck(a, b)) {
			return false;
		}

		a = a.toLowerCase();
		b = b.toLowerCase();
		
        int res = 0;
        for (int i = 0; i < a.length(); i++) {
            res ^= a.charAt(i);
            res ^= b.charAt(i);
        }
        
        return res == 0;
    }


}
